package sk.murin.animalproject

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import sk.murin.animalproject.entity.Animal
import sk.murin.animalproject.entity.AnimalGender
import sk.murin.animalproject.repository.AnimalRepository

@SpringBootTest
class SpringWebDbExampleKotlinApplicationTests(@Autowired val animalRepository: AnimalRepository) {

    @Test
    fun `test repository `() {
        var a = Animal(0, "rexo", "zeleny", AnimalGender.MALE, "pes", 5)
        a = animalRepository.save(a)
        assert(a.id != 0L)
        assert(animalRepository.existsById(a.id))
        val animal = animalRepository.findById(a.id).orElseThrow()
        assert(animal.name == a.name)
        assert(animal.color == a.color)
        assert(animal.gender == a.gender)
        assert(animal.type == a.type)
        assert(animal.age == a.age)
        animalRepository.deleteById(a.id)
        assert(!animalRepository.existsById(a.id))
    }
}
