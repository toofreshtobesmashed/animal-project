package sk.murin.animalproject.controller.animalgroup

class AnimalGroupNotFoundException(id:Long): NoSuchElementException("nebola najdena animal groupa s  id $id")