package sk.murin.animalproject.controller.animalgroup

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import sk.murin.animalproject.entity.AnimalGroup
import sk.murin.animalproject.service.AnimalGroupService
@Tag(name="Animal group rest controller")
@RestController
class AnimalGroupRestController(val service: AnimalGroupService) {

    @Operation(summary = "create new animal group ")
    @ApiResponses(
            value = [ApiResponse(
                    responseCode = "200",
                    description = "animal found",
                    content = [Content(mediaType = "application/json", schema = Schema(implementation = AnimalGroup::class))]
            ), ApiResponse(responseCode = "404", description = "animal not found")]
    )
    @PostMapping("/api/animalgroups")
    fun save(@RequestBody request: AnimalGroupSaveRequest): AnimalGroup {
        val a = AnimalGroup(0, request.name)
        return service.save(a)
    }

    @Operation(summary = "get all animalgroups")
    @GetMapping("/api/animalgroups")// toto zobrazuje v postmanovi
    fun getAll(): List<AnimalGroup> {
        return service.getAll()
    }

    @ExceptionHandler(AnimalGroupNotFoundException::class)
    fun handleNotFound(e: AnimalGroupNotFoundException): ResponseEntity<String> = ResponseEntity(e.message,
            HttpStatus.NOT_FOUND)

    @Operation(summary = "get specific animalgroup")
    @GetMapping("/api/animalgroups/{id}")
    fun get(@PathVariable id: Long): AnimalGroup = service.get(id)

    @Operation(summary = "delete specific animalgroup")
    @DeleteMapping("/api/animalgroups/{id}")
    fun deleteAnimal(@PathVariable id: Long): Boolean {
        service.deleteById(id)
        return id != 0L
    }
    @Operation(summary = "Adding animals to animal group")
    @PostMapping("/api/animalgroups/{groupId}/animals/{animalId}")// toto zobrazuje v postmanovi
    fun saveAnimalToGroup(@PathVariable groupId: Long, @PathVariable animalId: Long): AnimalGroup {
        return service.addAnimalToGroup(groupId, animalId)
    }

}