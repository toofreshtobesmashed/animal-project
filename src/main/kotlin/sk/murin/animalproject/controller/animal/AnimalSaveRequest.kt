package sk.murin.animalproject.controller.animal

import sk.murin.animalproject.entity.AnimalGender


class AnimalSaveRequest(

        val name: String,
        val color: String,
        val gender: AnimalGender,
        val type: String,
        val age: Int

)