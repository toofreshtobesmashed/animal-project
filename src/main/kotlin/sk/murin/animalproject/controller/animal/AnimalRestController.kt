package sk.murin.animalproject.controller.animal

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import sk.murin.animalproject.entity.Animal
import sk.murin.animalproject.service.AnimalService

@Tag(name = "Animal Rest Controller")
@RestController
class AnimalRestController(val animalService: AnimalService) {

    @Operation(summary = "create new animal REST")
    @ApiResponses(
            value = [ApiResponse(
                    responseCode = "200",
                    description = "animal found",
                    content = [Content(mediaType = "application/json", schema = Schema(implementation = AnimalService::class))]
            ), ApiResponse(responseCode = "404", description = "animal not found")]
    )
    @PostMapping("/api/animals")
    fun save(@RequestBody request: AnimalSaveRequest): Animal {
        val a = Animal(0, request.name, request.color, request.gender, request.type, request.age)
        return animalService.saveAnimal(a)
    }

    @Operation(summary = "get all animals")
    @GetMapping("/api/animals")// toto zobrazuje v postmanovi
    fun getAll(): List<Animal> {
        return animalService.getAnimals()
    }

    @ExceptionHandler(AnimalNotFoundException::class)
    fun handleNotFound(e: AnimalNotFoundException): ResponseEntity<String> = ResponseEntity(e.message,
            HttpStatus.NOT_FOUND)

    @Operation(summary = "get specific animal")
    @GetMapping("/api/animals/{id}")
    fun get(@PathVariable id: Long): Animal = animalService.getAnimal(id)

    @Operation(summary = "delete specific animal")
    @DeleteMapping("/api/animals/{id}")
    fun deleteAnimal(@PathVariable id: Long): Boolean {
        animalService.deleteById(id)
        return id != 0L
    }

    @Operation(summary = "edit animal age")
    @PatchMapping("/api/animals/{id}")
    fun editAnimalsAge(@PathVariable id: Long, @RequestBody request: AnimalAgePatchRequest) =
            animalService.editAnimalAge(id, request)
}