package sk.murin.animalproject.service
// http://localhost:8080/swagger-ui/index.html
import org.springframework.stereotype.Service
import sk.murin.animalproject.controller.animalgroup.AnimalGroupNotFoundException
import sk.murin.animalproject.entity.AnimalGroup
import sk.murin.animalproject.repository.AnimalGroupRepository
import sk.murin.animalproject.repository.AnimalRepository

@Service
class AnimalGroupService(private val animalGroupRepo: AnimalGroupRepository, private val animalRepository: AnimalRepository) {

    fun save(animalGroup: AnimalGroup): AnimalGroup {
        return animalGroupRepo.save(animalGroup)
    }

    fun get(id: Long): AnimalGroup {
        return animalGroupRepo.findById(id).orElseThrow { AnimalGroupNotFoundException(id) }
    }

    fun getAll(): List<AnimalGroup> {
        return animalGroupRepo.findAll()
    }

    fun deleteById(id: Long) {
        animalGroupRepo.deleteById(id)
    }

    //tu ziskavame aj animal aj animal group
    //a pridavam naraz do tabulky animal_group_animals
    fun addAnimalToGroup(groupId: Long, animalId: Long): AnimalGroup {
        val animalGroup = animalGroupRepo.getById(groupId)
        val animal = animalRepository.getById(animalId)

        //tu pridavam animal do arraylistu animals
        animalGroup.animals.add(animal)
        //tu to pridavam do db
        animalGroupRepo.save(animalGroup)
        return animalGroup
    }

}