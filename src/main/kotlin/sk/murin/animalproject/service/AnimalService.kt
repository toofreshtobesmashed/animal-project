package sk.murin.animalproject.service
// http://localhost:8080/swagger-ui/index.html
import org.springframework.stereotype.Service
import sk.murin.animalproject.controller.animal.AnimalAgePatchRequest
import sk.murin.animalproject.controller.animal.AnimalNotFoundException
import sk.murin.animalproject.entity.Animal
import sk.murin.animalproject.repository.AnimalRepository

@Service
class AnimalService(private val animalRepository: AnimalRepository) {

    fun saveAnimal(animal: Animal): Animal {
        return animalRepository.save(animal)
    }

    fun getAnimal(id: Long): Animal {
        return animalRepository.findById(id).orElseThrow { AnimalNotFoundException(id) }
    }

    fun getAnimals(): List<Animal> {
        return animalRepository.findAll()
    }

    fun deleteById(id: Long) {
        animalRepository.deleteById(id)
    }

    fun editAnimalAge(id: Long, request: AnimalAgePatchRequest) =
            animalRepository.save(getAnimal(id).also {
                it.age = request.age
            })

}