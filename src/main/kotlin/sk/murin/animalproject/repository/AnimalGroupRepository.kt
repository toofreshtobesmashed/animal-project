package sk.murin.animalproject.repository

import org.springframework.data.jpa.repository.JpaRepository
import sk.murin.animalproject.entity.AnimalGroup

interface AnimalGroupRepository : JpaRepository<AnimalGroup, Long>