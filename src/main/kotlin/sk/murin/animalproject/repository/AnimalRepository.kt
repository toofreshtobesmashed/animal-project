package sk.murin.animalproject.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import sk.murin.animalproject.entity.Animal
@Repository
interface AnimalRepository : JpaRepository<Animal, Long>
