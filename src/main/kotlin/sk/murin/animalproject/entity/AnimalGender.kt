package sk.murin.animalproject.entity

enum class AnimalGender {
    MALE,
    FEMALE
}
