package sk.murin.animalproject.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class AnimalGroup(@Id @GeneratedValue val id: Long,
                       val name: String,
                       @OneToMany val animals: MutableList<Animal> = mutableListOf()

)