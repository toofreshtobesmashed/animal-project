package sk.murin.animalproject.entity

import java.util.*
import javax.persistence.*

@Entity
data class Animal(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long,
        val name: String,
        val color: String,
        @Enumerated(EnumType.STRING)
        val gender: AnimalGender,
        val type: String,
        var age: Int,
        @Column(name = "last_update_at")
        val timestamp: Date = Date(System.currentTimeMillis())
)